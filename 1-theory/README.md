# Theory
This is a small test to evaluate how much you know about JAVA basics.

## Instructions 
Selected the answer you consider correct and write the literal A, B, C or D in the Answer line.

## Questions

### Question 1
Structuring a Java class such that only methods within the class can access its instance variables is referred to as:

A) platform independence

B) object orientation

C) inheritance

D) encapsulation

#### Answer: 

### Question 2
Which best describes what the `new` keyword does?

A) Creates a copy of an existing object and treats it as a new one

B) Creates a new primitive

C) Instantiates a new object

D) Switches an object reference to a new one

#### Answer: 

### Question 3
Which of the following statements about a default branch in a switch statement is correct?

A) All switch statements must include a default statement.

B) The default statement is required to be placed after all case statements.

C) Unlike a case statement, the default statement does not take a value.

D) A default statement can only be used when at least one case statement is present.

#### Answer: 

### Question 4
Which is not a true statement about an array?

A) An array expands automatically when it is full.

B) An array is allowed to contain duplicate values.

C) An array understands the concept of ordered elements.

D) An array uses a zero index to reference the first element.

#### Answer: 

### Question 5
Which type of loop is best known for its boolean condition that controls entry to the loop?

A) do-while loop

B) for (traditional)

C) for-each

D) while

#### Answer: 


### Question 6
Which of the following statements is not true?

A) An instance of one class may access an instance of another class’s attributes if it has a reference to the instance and the attributes are declared public.

B) An instance of one class may access package-private attributes in a parent class, provided the parent class is not in the same package.

C) Two instances of the same class may access each other’s private attributes.

D) An instance of one class may access an instance of another class’s attributes if both classes are located in the same package and marked protected.

#### Answer: 


### Question 7
Which of the following statements about inheritance is true?

A) Inheritance allows objects to access commonly used attributes and methods.

B) Inheritance always leads to simpler code.

C) All primitives and objects inherit a set of methods.

D) Inheritance allows you to write methods that reference themselves.

#### Answer: 

### Question 8
Which of the following exception types must be handled or declared by the method in which they are thrown?

A) `NullPointerException`

B) `Exception`

C) `RuntimeException`

D) `ArithmeticException`

#### Answer: 

### Question 9
What is not true about a String?

A) It can be created without coding a call to a constructor.

B) It can be reused via the string pool.

C) It is final.

D) It is mutable.

#### Answer: 

### Question 10
What is the main benefit of a lambda expression?

A) It allows you to convert a primitive to a wrapper class.

B) It allows you to change the bytecode while the application is running.

C) It allows you to inherit from multiple classes.

D) It allows you to write code that has the execution deferred.

#### Answer: 
