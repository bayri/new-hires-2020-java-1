# Code Review
Do you know how important the code-review process is? Sadly, many companies don't take care about it. But we are the exception. 

> Code review is systematic examination … of computer source code. It is intended to find and fix mistakes overlooked in the initial development phase, improving both the overall quality of software and the developers’ skills.

## Instructions 
1. Take a look at `Interview.java` file. Take special attention to the functional requirement.
2. Place comments wherever you consider the code could be improved. Some things you may want to prevent in the code are:
	1. Inconsistent design and implementation.
	2. Wrong use of variables and functions names.
	3. Unneeded complexity in a function or class.
	4. Any other stuff you consider could be made in a better way.
3. Place comments to ask any questions in case something is not clear for you in the code.
4. Remember to be polite in your comments.
