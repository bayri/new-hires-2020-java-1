# Hands-on JAVA project
This is the best way to demostrate how much you know about the language. 

## Instructions
In this project, you should be able to parse a HTML file (using a parser library or implementing your own parser), then extract a list of students, sort them alphabetically and printing the result. 
The HTML files are inside of `/files` directory. You will see 3 different files: `page1.html`, `page2.html`, `page3.html`. The parser should be able to parse all of them, and display **only** the name of the students. 

1. Clone this repository in your computer
2. Use the branch created for you. It should be given by your evaluator. 
3. After committing the changes you should push them, so that we can evaluate them.
4. If you have any issues with any of the steps above, don't hesitate to ask us.

We do not expect that you finish all the points because of time limitations. That's ok. Please choose wisely what you want to accomplish.

### Things to evaluate (10 pts):
- **Compilation** (1 pts): Does the code compile?
- **Execution** (1 pts): Can the code be executed without any error/exception?
- **Parser** (1 pts): Implementation of a third-party parser library or implementing your own parser to extract the list of students from the HTML file.
- **Sorting** (1 pts): Implementation of an efficient and clear algorithm to sort the students list. (Do not be afraid to use all the tools provided by the language).
- **Printing** (1 pts): The result was printed and is readable. 
- **Code style** (1 pts): Code style, conventions, standards, good practices...
- **Unit tests** (1 pts): Does the code contain unit test and coverage.
- **Result 1** (1 pts): The result for page1 is correct
- **Result 2** (1 pts): The result for page2 is correct
- **Result 3** (1 pts): The result for page3 is correct